# Nextcloud helm deploy

## Seed default values based on tag we use.

For chart version 2.9.2 which uses NC version 21.

    curl -O https://raw.githubusercontent.com/nextcloud/helm/nextcloud-2.9.2/charts/nextcloud/values.yaml

## Notes

### Get the nextcloud URL by running:

    export POD_NAME=$(kubectl get pods --namespace nextcloud -l "app.kubernetes.io/name=nextcloud" -o jsonpath="{.items[0].metadata.name}")
    echo http://127.0.0.1:8080/
    kubectl port-forward $POD_NAME 8080:80

### Get your nextcloud login credentials by running:

    echo User:     admin
    echo Password: $(kubectl get secret --namespace nextcloud nextcloud -o jsonpath="{.data.nextcloud-password}" | base64 --decode)

### Get a shell in nextcloud:

    kubectl exec -n nextcloud -ti deployment/nextcloud -- /bin/bash
    root@nextcloud-6ff6cdf587-vkkvw:/var/www/html# su -l -s /bin/bash www-data
    www-data@nextcloud-6ff6cdf587-vkkvw:~$ cd html/
    www-data@nextcloud-6ff6cdf587-vkkvw:~/html$ PHP_MEMORY_LIMIT=512M php occ status

### Expand storage volume

    kubectl -n nextcloud delete deployment/nextcloud
    kubectl -n nextcloud describe pvc/nextcloud-data
    > Used By:       <none>
    kustomize build kustomize/env | kubectl -n nextcloud apply -f -
    kubectl -n nextcloud describe pvc/nextcloud-data
    > Used By:       troubleshoot-68799bfbc
    kubectl -n nextcloud exec -ti deploy/troubleshoot -- /bin/bash
    root@troubleshoot$ umount /mnt
    root@troubleshoot$ resize2fs /dev/sda

### Complete upgrade in CLI

Nextcloud will do automatic updates of point-releases and will require you to finish the upgrade either in web console or in CLI. I prefer CLI.

1. Re-deploy helm chart with all probes disabled in values, or the Container won't remain Running because Nextcloud is down during upgrade mode.
2. Now you can enter shell and execute commands as in [the steps above](#get-a-shell-in-nextcloud).

```
www-data@nextcloud-6ff6cdf587-vkkvw:~/html$ PHP_MEMORY_LIMIT=512M php occ upgrade
Setting log level to debug
Turned on maintenance mode
...
Update successful
Turned off maintenance mode
Resetting log level
```

